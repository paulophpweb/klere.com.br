<?php
if($is_error):
?>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="alert alert-warning">
          <strong>Erro!</strong> <?php echo $error; ?>
        </div>
    </div>
</div>
<?php
else:
?>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                                                            
        <div class="box nopadding pav-categoryproducts clearfix">
            
            <div class="box-wapper">
                <div class="tab-nav">
                    <ul class="nav nav-tabs" role="tablist" id="instagram">
                        <li role="presentation" class="effect blue active first last">
                            <a href="#tab-instagram" aria-controls="tab-instagram" role="tab" data-toggle="tab" class="category_name box-heading">
                                <span style="cursor: pointer;">Instagram</span>
                            </a>
                        </li>
                    </ul>           
                </div>

                <!-- ========Content======== -->
                <div class="tab-content blue"> 
                    <div class="tab-pane  hasbanner clearfix active" id="tab-instagram">   
                          <div class="row">
                            <?php foreach ($images as $image): ?>
                            <div class="col-md-3 col-sm-4 col-xs-6 imageInstagram">
                                <img src="<?php echo $image; ?>" class="img-responsive"/>
                            </div>
                            <?php endforeach; ?>
                        </div>       
                    </div>  
                 </div>
            </div>      
        </div>
    </div>
</div>

<?php
endif;
?>
