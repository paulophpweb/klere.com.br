#!/bin/bash
echo "Modificando Permissões padrão"
sudo chmod -R 775 * && chmod -R 644 *.*
echo "Modificando system/storage/cache/ com 777"
sudo chmod -R 0777 system/storage/cache/
echo "Modificando system/storage/logs/ com 777"
sudo chmod -R 0777 system/storage/logs/
echo "Modificando system/storage/download/ com 777"
sudo chmod -R 0777 system/storage/download/
echo "Modificando system/storage/upload/ com 777"
sudo chmod -R 0777 system/storage/upload/
echo "Modificando system/storage/modification/ com 777"
sudo chmod -R 0777 system/storage/modification/
echo "Modificando image/ com 777"
sudo chmod -R 0777 image/
echo "Modificando image/cache/ com 777"
sudo chmod -R 0777 image/cache/
echo "Modificando image/catalog/ com 777"
sudo chmod -R 0777 image/catalog/