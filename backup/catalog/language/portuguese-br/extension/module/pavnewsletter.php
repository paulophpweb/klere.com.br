<?php


$_['entry_sign_up_for_newsletter'] = "Receba Novidades";

$_['entry_newsletter'] = "Newsletter";

$_['button_ok'] = "Ok";

$_['button_subscribe'] = "Enviar";

$_['default_input_text'] = "Seu email";

$_['valid_email'] = "Este email e inválido!";

$_['success_post'] = "Dados recebido com sucesso.";

$_['error_post'] = "Este email já esta cadastrado";

$_['description'] = "Fique por dentro do mundo da moda, coloque o seu email abaixo e receba novidades no seu email, e grátis.";

$_['description_fs'] = "Receba 15% de desconto em sua próxima compra. Seja o primeiro a saber das promoções, eventos especiais, novidades e muito mais.";

?>