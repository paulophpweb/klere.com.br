<?php

class ControllerExtensionModuleInstagram extends Controller {
    public function index() {

        $data['is_error'] = false;
        $limit = 0;
        
        $id_usuario_instagram = $this->config->get('instagram_module_id');
        $token = $this->config->get('instagram_module_token');
        $limit = $this->config->get('instagram_limit');
        if($id_usuario_instagram && $token){
            $mediaResponse = @file_get_contents("https://api.instagram.com/v1/users/".$id_usuario_instagram."/media/recent/?access_token=".$token."&count=".$limit);
            if($mediaResponse){
                $mediaResponse = json_decode($mediaResponse,true);
            }else{
              $data['is_error'] = true;
              $data['error'] = "Nenhum resultado encontrado.";  
            }

            if($data['is_error']){
                return $this->load->view('extension/module/instagram', $data);
            }else{
                $data["images"] = [];

                for($i=0; $i<count($mediaResponse['data']);$i++){
                    array_push($data["images"], $mediaResponse['data'][$i]['images']['standard_resolution']['url']);
                }
            }
            
        }else{
            $data['is_error'] = true;
            $data['error'] = "Token ou id do instragram não definido.";
        }

        return $this->load->view('extension/module/instagram', $data);
    }
}