<?php
if($is_error):
?>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="alert alert-warning">
          <strong>Erro!</strong> <?php echo $error; ?>
        </div>
    </div>
</div>
<?php
else:
?>

<div class="row">
    <?php foreach ($images as $image): ?>
    <div class="col-md-3 col-sm-4 col-xs-6 imageInstagram">
        <img src="<?php echo $image; ?>" class="img-responsive"/>
    </div>
    <?php endforeach; ?>
</div>

<?php
endif;
?>
