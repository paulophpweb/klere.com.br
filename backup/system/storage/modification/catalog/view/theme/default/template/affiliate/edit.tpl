<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
        <fieldset>
          <legend><?php echo $text_your_details; ?></legend>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-firstname"><?php echo $entry_firstname; ?></label>
            <div class="col-sm-10">
              <input type="text" name="firstname" value="<?php echo $firstname; ?>" placeholder="<?php echo $entry_firstname; ?>" id="input-firstname" class="form-control" />
              <?php if ($error_firstname) { ?>
              <div class="text-danger"><?php echo $error_firstname; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-lastname"><?php echo $entry_lastname; ?></label>
            <div class="col-sm-10">
              <input type="text" name="lastname" value="<?php echo $lastname; ?>" placeholder="<?php echo $entry_lastname; ?>" id="input-lastname" class="form-control" />
              <?php if ($error_lastname) { ?>
              <div class="text-danger"><?php echo $error_lastname; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-email"><?php echo $entry_email; ?></label>
            <div class="col-sm-10">
              <input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
              <?php if ($error_email) { ?>
              <div class="text-danger"><?php echo $error_email; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-telephone"><?php echo $entry_telephone; ?></label>
            <div class="col-sm-10">
              <input type="text" name="telephone" value="<?php echo $telephone; ?>" placeholder="<?php echo $entry_telephone; ?>" id="input-telephone" class="form-control" />
              <?php if ($error_telephone) { ?>
              <div class="text-danger"><?php echo $error_telephone; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-fax"><?php echo $entry_fax; ?></label>
            <div class="col-sm-10">
              <input type="text" name="fax" value="<?php echo $fax; ?>" placeholder="<?php echo $entry_fax; ?>" id="input-fax" class="form-control" />
            </div>
          </div>
        </fieldset>
        <fieldset>
          <legend><?php echo $text_your_address; ?></legend>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-company"><?php echo $entry_company; ?></label>
            <div class="col-sm-10">
              <input type="text" name="company" value="<?php echo $company; ?>" placeholder="<?php echo $entry_company; ?>" id="input-company" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-website"><?php echo $entry_website; ?></label>
            <div class="col-sm-10">
              <input type="text" name="website" value="<?php echo $website; ?>" placeholder="<?php echo $entry_website; ?>" id="input-website" class="form-control" />
            </div>
          </div>

					<div class="form-group required">
						<label class="col-sm-2 control-label" for="input-postcode"><?php echo $entry_postcode; ?></label>
						<div class="col-sm-10">
						  <input type="text" name="postcode" value="<?php echo $postcode; ?>" placeholder="<?php echo $entry_postcode; ?>" id="input-postcode" class="form-control" />
						  <?php if ($error_postcode) { ?>
						  <div class="text-danger"><?php echo $error_postcode; ?></div>
						  <?php } ?>
						</div>
					</div>
				
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-address-1"><?php echo $entry_address_1; ?></label>
            <div class="col-sm-10">
              <input type="text" name="address_1" value="<?php echo $address_1; ?>" placeholder="<?php echo $entry_address_1; ?>" id="input-address-1" class="form-control" />
              <?php if ($error_address_1) { ?>
              <div class="text-danger"><?php echo $error_address_1; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-address-2"><?php echo $entry_address_2; ?></label>
            <div class="col-sm-10">
              <input type="text" name="address_2" value="<?php echo $address_2; ?>" placeholder="<?php echo $entry_address_2; ?>" id="input-address-2" class="form-control" />
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-city"><?php echo $entry_city; ?></label>
            <div class="col-sm-10">
              <input type="text" name="city" value="<?php echo $city; ?>" placeholder="<?php echo $entry_city; ?>" id="input-city" class="form-control" />
              <?php if ($error_city) { ?>
              <div class="text-danger"><?php echo $error_city; ?></div>
              <?php } ?>
            </div>
          </div>
<!-- 
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-postcode"><?php echo $entry_postcode; ?></label>
            <div class="col-sm-10">
              <input type="text" name="postcode" value="<?php echo $postcode; ?>" placeholder="<?php echo $entry_postcode; ?>" id="input-postcode" class="form-control" />
              <?php if ($error_postcode) { ?>
              <div class="text-danger"><?php echo $error_postcode; ?></div>
              <?php } ?>
            </div>
          </div>
 -->
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-country"><?php echo $entry_country; ?></label>
            <div class="col-sm-10">
              <select name="country_id" id="input-country" class="form-control">
                <option value=""><?php echo $text_select; ?></option>
                <?php foreach ($countries as $country) { ?>
                <?php if ($country['country_id'] == $country_id) { ?>
                <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select>
              <?php if ($error_country) { ?>
              <div class="text-danger"><?php echo $error_country; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-zone"><?php echo $entry_zone; ?></label>
            <div class="col-sm-10">
              <select name="zone_id" id="input-zone" class="form-control">
              </select>
              <?php if ($error_zone) { ?>
              <div class="text-danger"><?php echo $error_zone; ?></div>
              <?php } ?>
            </div>
          </div>
        </fieldset>
        <div class="buttons clearfix">
          <div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a></div>
          <div class="pull-right">
            <input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-primary" />
          </div>
        </div>
      </form>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<script type="text/javascript"><!--
$('select[name=\'country_id\']').on('change', function() {
	$.ajax({
		url: 'index.php?route=affiliate/edit/country&country_id=' + this.value,
		dataType: 'json',
		beforeSend: function() {
			$('select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
		},
		complete: function() {
			$('.fa-spin').remove();
		},
		success: function(json) {
			if (json['postcode_required'] == '1') {
				$('input[name=\'postcode\']').parent().parent().addClass('required');
			} else {
				$('input[name=\'postcode\']').parent().parent().removeClass('required');
			}

			html = '<option value=""><?php echo $text_select; ?></option>';

			if (json['zone'] && json['zone'] != '') {
				for (i = 0; i < json['zone'].length; i++) {
					html += '<option value="' + json['zone'][i]['zone_id'] + '"';

					if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
						html += ' selected="selected"';
					}

					html += '>' + json['zone'][i]['name'] + '</option>';
				}
			} else {
				html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
			}

			$('select[name=\'zone_id\']').html(html);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('select[name=\'country_id\']').trigger('change');
//--></script>

					<script type="text/javascript">
						$(function(){
							$('#input-postcode').blur(function(){
								var cep = $.trim($('#input-postcode').val().replace('-', ''));
				
								$.getJSON("https://viacep.com.br/ws/"+cep+"/json/", function(data) {
									var resultadoCEP = data;
									if(resultadoCEP["logradouro"] != "" && resultadoCEP["logradouro"] != undefined){
										$('#input-address-1').val(unescape(resultadoCEP["logradouro"]));
										$('#input-address-2').val(unescape(resultadoCEP["bairro"]));
										$('#input-city').val(unescape(resultadoCEP["localidade"]));

										$('#input-country').find('option[value="30"]').attr('selected', true);
										$.post('index.php?route=account/register/estado_autocompletar&estado=' + unescape(resultadoCEP['uf']), function(zone_id){
											$.ajax({
												url: 'index.php?route=account/account/country&country_id=30',
												dataType: 'json',
												beforeSend: function() {
													$('#input-country').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
												},
												complete: function() {
													$('.wait').remove();
												},			
												success: function(json) {
													if (json['postcode_required'] == '1') {
														$('input[name=\'postcode\']').parent().parent().addClass('required');
													} else {
														$('input[name=\'postcode\']').parent().parent().removeClass('required');
													}
			
													var html = '<option value=""><?php echo $text_select; ?></option>';
			
													if (json['zone'] != '') {
														for (i = 0; i < json['zone'].length; i++) {
															html += '<option value="' + json['zone'][i]['zone_id'] + '"';
															
															if (json['zone'][i]['zone_id'] == zone_id) {
																html += ' selected="selected"';
															}
											
															html += '>' + json['zone'][i]['name'] + '</option>';
														}
													} else {
														html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
													}
			
													$('#input-zone').html(html);
												}
											});
										});
									} else if (resultadoCEP.erro) {
										$('#input-postcode').val('');
										alert('CEP não encontrado!');
										$('#input-postcode').focus();
									}
								});
							});
						});
					</script>
				

					<script type="text/javascript"><!--
						function vCPF(cpf) {
							cpf = cpf.replace(/[^\d]+/g,'');
							
							if(cpf == '' || cpf.length != 11) return false;
							
							var resto;
							var soma = 0;
							
							if (cpf == "00000000000" || cpf == "11111111111" || cpf == "22222222222" || cpf == "33333333333" || cpf == "44444444444" || cpf == "55555555555" || cpf == "66666666666" || cpf == "77777777777" || cpf == "88888888888" || cpf == "99999999999" || cpf == "12345678909") return false;
							
							for (i=1; i<=9; i++) soma = soma + parseInt(cpf.substring(i-1, i)) * (11 - i);
							resto = (soma * 10) % 11;
							
							if ((resto == 10) || (resto == 11))  resto = 0;
							if (resto != parseInt(cpf.substring(9, 10)) ) return false;
							
							soma = 0;
							for (i = 1; i <= 10; i++) soma = soma + parseInt(cpf.substring(i-1, i)) * (12 - i);
							resto = (soma * 10) % 11;
							
							if ((resto == 10) || (resto == 11))  resto = 0;
							if (resto != parseInt(cpf.substring(10, 11) ) ) return false;
							return true;
						}
						function vCNPJ(cnpj) {
							cnpj = cnpj.replace(/[^\d]+/g,'');

							if(cnpj == '' || cnpj.length != 14) return false;

							if (cnpj == "00000000000000" || cnpj == "11111111111111" || cnpj == "22222222222222" || cnpj == "33333333333333" || cnpj == "44444444444444" || cnpj == "55555555555555" || cnpj == "66666666666666" || cnpj == "77777777777777" || cnpj == "88888888888888" || cnpj == "99999999999999") return false;

							var tamanho = cnpj.length - 2
							var numeros = cnpj.substring(0,tamanho);
							var digitos = cnpj.substring(tamanho);
							var soma = 0;
							var pos = tamanho - 7;
							
							for (i = tamanho; i >= 1; i--) {
							  soma += numeros.charAt(tamanho - i) * pos--;
							  if (pos < 2)
									pos = 9;
							}
							
							var resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
							
							if (resultado != digitos.charAt(0)) return false;

							tamanho = tamanho + 1;
							numeros = cnpj.substring(0,tamanho);
							soma = 0;
							pos = tamanho - 7;
							
							for (i = tamanho; i >= 1; i--) {
							  soma += numeros.charAt(tamanho - i) * pos--;
							  if (pos < 2) pos = 9;
							}
							
							resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
							
							if (resultado != digitos.charAt(1)) return false;

							return true; 
						}
						$("input[placeholder]").each( function () {
						    if ($(this).attr("placeholder").indexOf("CPF")>=0) {
								var cId = $(this).attr("id");
								$(this).mask('000.000.000-00', {
									onComplete: function(val, e, field, options) {
										if (!vCPF(val)) {
											alert("Digite um CPF válido!");
											$("#"+cId).val("");
										}
									},
									placeholder: "___.___.___-__"
								});
						    }
						    if ($(this).attr("placeholder").indexOf("CNPJ")>=0) {
								var cnId = $(this).attr("id");
								$(this).mask('00.000.000/0000-00', {
									onComplete: function(val, e, field, options) {
										if (!vCNPJ(val)) {
											alert("Digite um CNPJ válido!");
											$("#"+cnId).val("");
										}
									},
									placeholder: "__.___.___/____-__"
								});
						    }
						    if ($(this).attr("placeholder").indexOf("Data")>=0) {
								$(this).mask('00/00/0000', {placeholder: "__/__/____"});
						    }
						    if ($(this).attr("placeholder").indexOf("Telefone")>=0 || $(this).attr("placeholder").indexOf("Fax")>=0 || $(this).attr("placeholder").indexOf("Celular")>=0) {
								var masks = ['(00) 00000-0000', '(00) 0000-00009'];
								$(this).mask(masks[1], {
									onKeyPress: function(val, e, field, options) {
										field.mask(val.length > 14 ? masks[0] : masks[1], options) ;
									}, 
									placeholder: "(__)____-____"
								});
						    }
						    if ($(this).attr("placeholder").indexOf("CEP")>=0) {
								$(this).mask('00000-000', {placeholder: "_____-___", clearIfNotMatch: true});
						    }
						});
					//--></script>
				
<?php echo $footer; ?>