<?php
$_["heading_title"] = "Instagram";
$_["instagram_username"] = "Usuário";
$_["instagram_username_placeholder"] = "Coloque o seu usuário do instagram";
$_["limit_text"] = "Número de fotos";
$_["limit_text_placeholder"] = "Número de fotos que irão aparecer";
$_['text_module']      = 'Módulos';
$_['text_success']     = 'Successo: Instagram alterado com sucesso!';
$_['text_edit']        = 'Editar módulo instagram';
$_["text_enabled"] = "Habilitado";
$_["text_disbaled"] = "Desabilitado";
$_['module_name_title'] = "Nome do Módulo";
$_['module_name_placeholder'] = "Especifique o nome do módulo, deixe vazio para não mostrar nada";
$_['module_token_title'] = "Token do Instagram";
$_['module_token_placeholder'] = "Especifique o token";
$_['module_id_title'] = "Id do usuário";
$_['module_id_placeholder'] = "Especifique o id do usuário do instagram";
$_['error_title'] = "Mensagem de erro";
$_['error_placeholder'] = "Especifique a mensagem de erro a ser exibida quando o usuário for privado, deixe vazio para mostrar o padrão";
$_['stylesheet'] = "Folha de estilos personalizada";

// Entry
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Aviso: Você não tem permissão para modificar o módulo de conta!';