<?php
$_["heading_title"] = "Instagram";
$_["instagram_username"] = "Username";
$_["instagram_username_placeholder"] = "Enter your Instagram placeholder";
$_["limit_text"] = "No of media";
$_["limit_text_placeholder"] = "No of media you want to show";
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified Instagram module!';
$_['text_edit']        = 'Edit Instagram Module';
$_["text_enabled"] = "Enabled";
$_["text_disbaled"] = "Disabled";
$_['module_name_title'] = "Module Name";
$_['module_name_placeholder'] = "Specify module name, leave empty to show nothing";
$_['module_token_title'] = "Token do Instagram";
$_['module_token_placeholder'] = "Especifique o token";
$_['module_id_title'] = "Id do usuário";
$_['module_id_placeholder'] = "Especifique o id do usuário do instagram";
$_['error_title'] = "Error message";
$_['error_placeholder'] = "Specify error message to display when user is private, leave empty to show default";
$_['stylesheet'] = "Custom Stylesheet";

// Entry
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify account module!';