$(function(){
  // animacao para adicionar o produto ao carrinho
    $('.product-layout .fa-shopping-cart').parent().on('click', function () {
        var cart = $('#cart');
        var imgtodrag = $(this).closest('.product-thumb').find('img');
        if (imgtodrag) {
            var imgclone = imgtodrag.clone()
                .offset({
                    top: imgtodrag.offset().top,
                    left: imgtodrag.offset().left
                })
                .css({
                    'opacity': '0.5',
                    'position': 'absolute',
                    'z-index': '100'
                })
                .appendTo($('body'))
                .animate({
                    'top': cart.offset().top + 10,
                    'left': cart.offset().left + 10,
                    width:100
                }, 1000);

            imgclone.animate({
                'width': 0,
                'height': 0
            }, function () {
                $(this).detach();
                $('#cart button').eq(0).click();
            });
        }
    });
    $("#zoom_01").elevateZoom({
      zoomType				: "inner",
      cursor: "crosshair"
    });
    $('.thumb_image1').click(function(){

  		small_image = $(this).attr('name');
  		large_image = $(this).attr('id');
  		$('.main_image').attr('src',small_image);
  		$('#zoom_01').attr('src',small_image);
  		$('#zoom_01').attr('data-zoom-image',large_image);
  		smallImage = small_image;
  		largeImage = large_image;
   	    var ez =   $('#zoom_01').data('elevateZoom');
  	    ez.swaptheimage(smallImage, largeImage);

  	});
});
