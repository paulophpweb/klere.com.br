<?php
// HTTP
define('HTTP_SERVER', 'http://teste.klere.com.br/admin/');
define('HTTP_CATALOG', 'http://teste.klere.com.br/');

// HTTPS
define('HTTPS_SERVER', 'http://teste.klere.com.br/admin/');
define('HTTPS_CATALOG', 'http://teste.klere.com.br/');

// DIR
define('DIR_APPLICATION', getcwd().'/../admin/');
define('DIR_SYSTEM', getcwd().'/../system/');
define('DIR_IMAGE', getcwd().'/../image/');
define('DIR_LANGUAGE', getcwd().'/../admin/language/');
define('DIR_TEMPLATE', getcwd().'/../admin/view/template/');
define('DIR_CONFIG', getcwd().'/../system/config/');
define('DIR_CACHE', getcwd().'/../system/storage/cache/');
define('DIR_DOWNLOAD', getcwd().'/../system/storage/download/');
define('DIR_LOGS', getcwd().'/../system/storage/logs/');
define('DIR_MODIFICATION', getcwd().'/../system/storage/modification/');
define('DIR_UPLOAD', getcwd().'/../system/storage/upload/');
define('DIR_CATALOG', getcwd().'/../catalog/');

// DB
define('DB_DRIVER', 'mpdo');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'paulopha_klere');
define('DB_PASSWORD', 'powman220187');
define('DB_DATABASE', 'paulopha_klere');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');

