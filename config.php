<?php
// HTTP
define('HTTP_SERVER', 'http://klere.dev/');

// HTTPS
define('HTTPS_SERVER', 'http://klere.dev/');

// DIR
define('DIR_APPLICATION', '/Volumes/ARQUIVOS/Projetos/2017/klere.com.br/catalog/');
define('DIR_SYSTEM', '/Volumes/ARQUIVOS/Projetos/2017/klere.com.br/system/');
define('DIR_IMAGE', '/Volumes/ARQUIVOS/Projetos/2017/klere.com.br/image/');
define('DIR_LANGUAGE', '/Volumes/ARQUIVOS/Projetos/2017/klere.com.br/catalog/language/');
define('DIR_TEMPLATE', '/Volumes/ARQUIVOS/Projetos/2017/klere.com.br/catalog/view/theme/');
define('DIR_CONFIG', '/Volumes/ARQUIVOS/Projetos/2017/klere.com.br/system/config/');
define('DIR_CACHE', '/Volumes/ARQUIVOS/Projetos/2017/klere.com.br/system/storage/cache/');
define('DIR_DOWNLOAD', '/Volumes/ARQUIVOS/Projetos/2017/klere.com.br/system/storage/download/');
define('DIR_LOGS', '/Volumes/ARQUIVOS/Projetos/2017/klere.com.br/system/storage/logs/');
define('DIR_MODIFICATION', '/Volumes/ARQUIVOS/Projetos/2017/klere.com.br/system/storage/modification/');
define('DIR_UPLOAD', '/Volumes/ARQUIVOS/Projetos/2017/klere.com.br/system/storage/upload/');

// DB
define('DB_DRIVER', 'mpdo');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'klere');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
